# STRUCTURE #
* ** HL7 RIM**: this folder contains all the information related to HL7 RIM.
*  * RDF: contains an rdf file with the HL7 RIM schema and an rdf file with dump with 80 fictitious patients.
*  * SQL: contains the SQL schema of the HL7 RIM relational database.
* **SPARQL queries**: contains a txt file with all the SPARQL queries of the elegibility criteria mentioned on the article.
* **images**: contains the images of the article.

# SUMMARY #
## Background and Objectives 
Post-genomic clinical trials require the participation of multiple institutions, and collecting data from several hospitals, laboratories and research facilities. This paper presents a standard-based solution to provide a uniform access endpoint to patient data involved in current clinical research.
## Methods 
The proposed approach exploits well-established standards such as HL7 or SPARQL and SNOMED CT, LOINC and HGNC terminologies. A novel mechanism to exploit semantic normalization among HL7-based data models and biomedical ontologies has been created by using semantic web technologies.
## Results 
Different types of queries have been used for testing the semantic interoperability solution described in this paper. The execution times obtained in the tests, enable the development of end user tools within a framework that requires efficient retrieval of integrated data. 
## Conclusions 
The proposed approach has been successfully tested by final application prototypes within the INTEGRATE and EURECA EU projects. These applications have been deployed and tested for: (i) trial recruitment, (ii) trial feasibility, (iii) cohort selection, and (iv) retrospective analysis; exploiting semantically interoperable access to clinical patient data from heterogeneous data sources.